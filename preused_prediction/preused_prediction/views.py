from django.shortcuts import render

def home(request):
    return render(request, 'index.html')

def prediction(request):
    return render(request,'prediction.html')

